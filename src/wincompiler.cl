cd src
gfortran -fno-optimize-sibling-calls -g -O2 -mtune=native -Wall -pedantic -flto=10 -c BDATpro.f -o BDATpro.o
gfortran -fno-optimize-sibling-calls -g -O2 -mtune=native -Wall -pedantic -flto=10 -c FormTarife.f -o FormTarife.o
gfortran -fno-optimize-sibling-calls -g -O2 -mtune=native -Wall -pedantic -flto=10 -c Koeff.f -o Koeff.o

gcc -I"C:\Program Files\R\R-4.0.1\include" -DNDEBUG   -I/usr/local/include  -g -O2 -Wall -pedantic -mtune=native -Werror=format-security -Wp,-D_FORTIFY_SOURCE=2 -fexceptions -fstack-protector-strong -Werror=implicit-function-declaration -flto=10 -c rBDAT_init.c -o rBDAT_init.o

gfortran -fno-optimize-sibling-calls -g -O2 -mtune=native -Wall -pedantic -flto=10 -c vBDATpro.f -o vBDATpro.o

gcc -shared -g -O2 -Wall -pedantic -mtune=native -Werror=format-security -Wp,-D_FORTIFY_SOURCE=2 -fexceptions -fstack-protector-strong -flto=10 -L"C:/Program Files/R/R-4.0.1/bin/x64" -lR -o rBDAT.dll BDATpro.o FormTarife.o Koeff.o rBDAT_init.o vBDATpro.o -lgfortran -lm -lquadmath

