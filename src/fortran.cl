gfortran --pedantic -O2 -Wall -c BDATpro.f 2> compilerout.txt
gfortran --pedantic -O2 -Wall -c Formtarife.f 2> compilerout.txt
gfortran --pedantic -O2 -Wall -c Koeff.f 2> compilerout.txt
gfortran --pedantic -O2 -Wall -c vBDATpro.f 2> compilerout.txt

gfortran -fsanitize=undefined -fno-optimize-sibling-calls -fpic -g -O2 -mtune=native -c BDATpro.f -o BDATpro.o 2> compileroutall.txt
gfortran -fsanitize=undefined -fno-optimize-sibling-calls -fpic -g -O2 -mtune=native -c FormTarife.f -o FormTarife.o 2> compileroutall2.txt
gfortran -fsanitize=undefined -fno-optimize-sibling-calls -fpic -g -O2 -mtune=native -c Koeff.f -o Koeff.o 2> compileroutall3.txt
gfortran -fsanitize=undefined -fno-optimize-sibling-calls -fpic -g -O2 -mtune=native -c vBDATpro.f -o vBDATpro.o 2> compileroutall4.txt
