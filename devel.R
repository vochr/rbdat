if(!require("packager")){
  remotes::install_gitlab("fvafrcu/packager")
  require("packager")
}
packager::provide_news_rd()
roxygen2::roxygenise()
packager:::sys_build()
packager:::sys_install()
