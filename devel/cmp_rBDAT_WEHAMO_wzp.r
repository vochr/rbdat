#' check rBDAT vs. WEHAM
rm(list=ls())
gc()
pin <- "H:/FVA-Projekte/P01612_KNOW/Daten/aufbereiteteDaten/run_weham/WEHAM_SzBasis_2099/WEHAM_OUTPUTXX40.mdb"

require(RODBC)
require(rBDAT)
con <- RODBC::odbcConnectAccess(pin)
wzp <- RODBC::sqlFetch(con, sqtable = "wehamo_wzp")
x_ba <- RODBC::sqlFetch(con, sqtable = "x_Ba")
RODBC::odbcClose(con)

sort(unique(wzp$Ba))
wzp$Ba <- ifelse(wzp$Ba %in% c(110, 111), 110, wzp$Ba)
uba <- c(10, 20, 30, 40, 100, 110)
wzp <- subset(wzp, Ba %in% uba & PJahr == 2012 & PArt == 0)
wzp <- merge(wzp, x_ba[, c("ICode", "KurzD", "Zu_BaBDat20")], by.x = "Ba", by.y = "ICode")

pdf("C:/temp_cv/cmp_WEHAM_rBDAT.pdf")
for(b in uba){
  # b <- 30
  swzp <- subset(wzp, Ba == b)
  swzp <- swzp[complete.cases(swzp),]
  swzp$rbdatVolR <- getAssortment(list(spp=swzp$Zu_BaBDat20, D1=swzp$Bhd/10, H1=1.3, 
                                         D2=swzp$D03/10, H2=0.3*swzp$Hoe/10, 
                                         H=swzp$Hoe/10, 
                                         Hkz = swzp$Kh, Skz=swzp$Kst), 
                                    value = "Vol")[[2]]
  swzp$rbdatBM <- getBiomass(list(spp=swzp$Zu_BaBDat20, D1=swzp$Bhd/10, H1=1.3, D2=swzp$D03/10, 
                                  H2=0.3*swzp$Hoe/10, H=swzp$Hoe/10)) # Biomass
  head(swzp)
  plot(VolR ~ rbdatVolR, data=swzp, pch=16, col=rgb(0,0,0,0.1), main=paste0("BA=",b))
  abline(0, 1, col="red")
  plot(Biomasse ~ rbdatBM, data=swzp, pch=16, col=rgb(0,0,0,0.1), main=paste0("BA=",b))
  abline(0, 1, col="red")
  
}
dev.off()
