b0 <- 0.09428
b1 <- 10.26998
b2 <- 8.13894
b3 <- 0.55845
k1 <- 400
k2 <- 8
c0 <- 0.87633
c1 <- 0.98279
a <- 0.31567
b <- 1.63335

# d13 <- 100
d13 <- 191.5
# d13 <- 184.2
d13s <- 94

# d03 <- 0.35*d13
d03 <- 68.5
# d03 <- 66.1
d03s <- d03 + (c0 * d13s^c1) - (c0 * d13^c1)
# d03s <- d13s * d03/d13

h <- 32.7
# h <- 32.6
hs <- h + (a + b/d13s)^-3 - (a + b/d13)^-3

## biomasse mit Standard-Marklund-Funktion
(B <- b0 * exp(b1 * d13 / (d13 + k1)) * exp(b2 * d03/(d03 + k2)) * h^b3)

## biomasse am Schwellendurchmesser d13s
(Bs <- b0 * exp(b1 * d13s / (d13s + k1)) * exp(b2 * d03s/(d03s + k2)) * hs^b3)

(B <- Bs * (1 + b1*k1 / (d13s + k1)^2 * (d13 - d13s) + 
              b2*k2 / (d03s + k2)^2 * (d03 - d03s) + 
              b3/hs * (h - hs)))

rBDAT::getBiomass(list(spp=17, D1=d13, H1=1.3, D2=d03, H2=0.3*h, H=h))
